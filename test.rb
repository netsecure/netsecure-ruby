require 'test/unit'
require 'netsecure'
require 'shoulda'
require 'json'

begin
=begin
  result = Netsecure.Transaction.post(
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'SALE',
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    :TotalAmount => "1.00",
    :EntryMode => "KEYED",
    :CardNum => "5555555555554444",
	:CVV2 => "321",
    :CardExpMonth => "12",
    :CardExpYear => "18",
    :CardName => "Test Holder",
    #optional fields
    :Phone      => "800-555-1234",
    :Address    => "123 Main Street",
    :PostalCode => "12345",
    :Latitude   => "51.028642",
    :Longitude  => "-113.993563",
    #base64 encoded
    :Signature  => "iVBORw0KGgoAAAANSUhEUgAAAhcAAABkCAIAAAC6koVZAAAgAElEQVR4nO2deVxUVf/Hzwy7bIIgAikIuCACIoiAgiYuKCCKZglqBrg8RZnlEmU/9NFAsygT7UmFsnLDLCDRQPBJEVBBxwVEEhRUUAFBUGAUGH5/HDvPbdY7dy7MMHzfr/ljGObOnHtn5nzP+S6fL6erqwsBAAAAACM0lT0AAADUHPP5u/Cd+EjfwPF2lqYGyh0PwC4c2IsAANCt1De1OUYmkz9LkyLMjPWUOB6AXcCKAADQ7dQ3tT15xk/OLN574hp+BGyJ2gBWBACAngP2JeoHWBEAAHqU+qY2hBCxJWBIejtgRQAAUAJUW1KwI8zB2kTZIwIYwlX2AAAA6IuYGeuZGevFR/oihMK3ZjxoeKbsEQEMASsCAIDSCBxvZ2dpfPtB05zYVDAkvRSwIgAAKA1LU4PUTXPAkPRqeqUVMZ+/y3z+LuxXBQCgV0M1JMEbfkvLLxcIIFjbm+iVVgQAAHUCG5KJo62rapujEjItFuyGNWIvAqwIAADKx9LU4PAnwdui/DyGWyCEHCOTj/x5EzYlvYLeZ0Va+e3KHgLLmM/fte/kNbXxCCvob1RjX6WafdCso6OlERHgfDJu/q+xIQih6MSco2fLlD0oQDa9z4qo2RcLC9XFJOW6LN8PUwy+Go6RyeXVjcoeC8tQP2gwJ9LxHGm5LcoPIRSdmPNH4Z22Fx3KHhEgjV5mRVr57Wv2nFH2KNgnfIojokwx5NZn55rkzGJlD6FbwB80QigmKRdSkiSBNyWlSREIocXbTsQdPK/sEQHS6GVWRM02IoQN4d4FO8KWzXIRejwmKTcg5pf4QxdKKuuVMrAeJjLAGd/Ze+KaWvq1NoR7lyZFFOwIg9xWmZgZ62VsCdXU4H6XcfXizQfKHg4gkd5kRdR1I4JxsDaJi/AtTYogt4IdYSE+DjWPWxKOFU1ec6QvbE28R1kpewjdjpmxnoO1CRRJ0MFzpOXqee5dXejb41eUPRZAIr3GirTy2xfGH0eU5apagmUh8M3B2uTb96YejAl8Y/LIgf37IYRiknLVO6E+2Mt+5GBTZY+iJ4BqO5qsDnVHCB0/f1st96bqQa+xIjaL9uSX1PTX1wn727PcF9DS1Jjmbrsz2v/6nqUFO8JIQr266g5xuZw1r43D9zsFAuUOpjugnhQYEjpoaWooewiADHqBFckrqcb5LZEBzsdiQ1zszJU9IiXA5XIcrE1wQv0gE/3sy1U4Dq9+C7RgL3t8R81iYB/M80AIJabxqA+CIQHUABW1Is/aXuzPKg6PP24+f9ec2FSE0OfLJsVF+PZNE0LAuStZ2+aTOLxjZLKaGRIulzN3wjCEUGIaT51qg4K87MQGisGQAL0dlbMiVypqP/zuz9HLfliz50zWpSqE0PiRlmsXjAudOIzL5Sh7dCqBpakBjsMreyDdReB4O4TQ42Z+cuZ1ZY+FNZyHmuNA8Ve/Xuro/IezjmpI3v4mW51sJ9AXUCEr0spv/+T73Gnrj/54qqSF325v1X/jEh/et0vS/z133QJPY30dZQ+wW/B2tEIIld1vkPdAlW0Px/iMCCTGrmbbkdWh7oPNDXN4VRdE8laxIbEZaHSuuFqlbCconwIyURUrglOw9mRc09bkzp0wLHXTnPPfhL8z2+0Vc0P13oI4WPdHCOWVVCt7IKyh+BmRGLuabUe0NDUmuw7u6hJ/cSxNDT5d5I0QOpZ7S2izonTUUk0AYAuVsCJ5JdUhsak4Betk3Pw9q6dPcLJW9qB6CB8na6ReVoSVMwr2sh9sbojUbjsi/eIEe9nbDDQqqaoX3awoHXVVEwAUR8lWJK+kOiT2tzmxqVcqakfZDOiDKVgTnKwQQkVlD9VGLAif0fXbde0dnYxfhMvlzPZ2QGq3HSEfd0OzGB8Rl8vxc3lF0mZFKeCOtkh91QQAxVGaFSH2I7+kxqif9toF49L/PbevmRCEkKWpwTBrkxcdghxelVwHquwK3dLUACH0tK29qeWFIq8T4vMy5VedtiOWpgaLp4560SF4d1eO2NJRVdub4o62+L5SKnjU5qNXYzRlPqOV3370bJl06ZFJLq+4Dxs029veydZM5gs+amxZu+fMycI7CCGjftorglxXBLqqa/CcDtPdbW9VN6bnVwSNt6d/FC6niAxwNjXU7bahKRM3Bwt8B29HokPGKnEwuGIpPtI3cLwdNpOMiQxwPnqmLOtS1e7feaInRd3JqULBHQ77uyzfjxA6eras5z8Ftf+eqwGyrYjNoj0yn3Pm2v0z1+4nHCvCf4ZPcXx1zJBgL3vRwPjRM2UfJ+c+aXkO9oMQ4mO/K52Xdamy7UWHnrbsTwSD7XofSYD+Oad02SxXHS3lzKrYhCCEYpJy9564lrppjiKGxMnW7LMI3w+/+/Ozgxc8R1h6jrSk/pe6k2OQhkdnzYehbxHJcxLTePN8hytoROWlT33Peymy56zSpIjkzOuTXQbbWfaX9Bxe+aP0gorTV+7WPmlFCB04XXrgdKmU15ztbb8tapLK5qr2MG4OFoPNDe/VPc3hVcm1HUEISflQlA5bDhCjftoVNU9OXa6U9+KwRWlShGNkMkJIV0sD1wYqaEiWTHPKL6k+du7Wh9/9mRk/v5+uFltDpWlCEEIxSbkxSbnkTzruhMfNfMXPnRmq/D0HZFsRM2O9dQs8pT9nmrvtNHdbgaCr4Slf0NV1/HzF0bNlRX89En3mbG/72d4OIT4ODMerFuC1bWlSBLGjs70ddqXz6Du1VNxZPHPc0JOFd/4ovPPm9NGKv9osT7vDf96U1+PHImbGevGRvjFJuROdX7l1v5EVQ/LFism8itqb9xrYddZhE/LL/812spHmW37yjJ+cWbz3xDXyCNWdIMmXMMhEH9dFHvgoUJLlE/1uA2oPXf8JHbhcDv7qRAQ4RwQ4i83oMDXUhZ2pKMSp1dTynI6LT8WdxaOHmp0svPNT9g1WrMhsb/vDf96U1+PHLoHj7b46dimHV7X7vWnbUy7KnExlYqCn/fFCr6iEzMQ0XsQMZxa3IwghJxsz6ZO4mbFeXIQvlvbCUN0JVF8Cdnzh+wc/Dnxr+x+4LlK65XOMTP41NsR9mAW75wWoJt2Yo0UVOSc3MCEEQdf/UnTcHCx8nKxa+O3xhy7QOVbFncXhU0ZpcDlXb9ddqahV/NWwx6+F3y5vGhuLWJoazPCw7epCtx88YavIHJfoKzGVmfrDJNLRJfve2hbl5zH8ZWpDTFLu299k4/ukLlJK1hwR5gndlKZOKdqAFFSi6rBvcrHsH5Vl/woagxD6Iav4+p066QeSH7DKOoutzQz83YYghH7KvsHKC+LakfT8ClZejRkkB5c6mSoinkhK9BPTeCpSLcTlcgb27xcR4Hwybj7uk4btJXmCTMtnZqxXmhTx7hw3pPD1AXoLYEWUhtCcGDBu6NSxNp2CrvX7zkpvQqVS7qzUvFtiZ4ol00YjhLKKKp+3M689JODaEezxU/zVCHKJRFHrQ4O97KeOtcHRZkUmymAv+zH2Ax8389ltLc5KXgPuk4btJab07mP+iw5i+SSduJmx3rJZLnaWxopfHxWP/wEYsCJKo+BGjZBcUlyEr7Ymt7Ds4ZEzN6UcqDruLPP5u5Z9lSV2pvB3GzLIRP9hY8uNKhY6xsvr8ZMLmur61PpQLpeTsHKy4iq8+HVYbC2OO4Gy2Jol2Ms+Mdof38dOKjoWFFeZIIRuP2hi3J2T2t5UFRZMgCTAiiiNh40tT579Y2U9dJDxu3PGIoQ2/1wguui+UlGL184IobyvF3oMH9RjQ5VEwY4whNDtB00uy/f/++d86pyiqcH1GGGBELpSIcNBRxP6Hj+x4EtHbriDPZH3oMaopDDd3Rb9vYlkS4VXimI8A3DjehZdSVwu5/XJI8mf36Tyfj9f8cXylxZ0ZypP0oEkgY1xd86jZ8tIe1OlL5gAKYAVYcLmAwXd1Px8Vaj7YHPDuqY2oUV3Xkn1tPVHX97/eqGDlYkq/K4crE1wH1+E0M5U3pzYVOoU7zLUHCHEbNIXhb7HTxRSNkjAQeNXXYfgP4ViVJIgjjW8+WArQCJFMV5eyEaB8Q5ACounjmp8yo9KyNx74trBmCCE0N4T1478eVP6u3z0hifpzomNN823w3vuAzGBfVAYqXcBVkRuzOfv+ua3y1EJmd3R0lVPWzMuwhf9c9Hdym/f+GM+Qmh5oEtpUoSKmBAMtY/v7QdN0z/65fOUi1iHEUuY8MpZSNPC0PT4SSLv64WlSRHUoPHJwtv4XzTj9m4OFjM8bFv47fGHX9p4VgIk0hXj5QK7yCaOtma8A5DCh/PHbYvyMzfW25XOO1l4+9fYEIRQdGKO9B/Cm9NGk+6cMUm5NC+U6qeQAASwIsyJTsxRsOnCvbpm0QeFFt3YO4w1j1eHeqhgtjTp4+sx3KKjU7A9pfDVNUfS8stH25ohhG7ea2ArRko8fnsyrjEI2psavsxqJUFj0gWdbC9ksv51T2oYg60ACYsijJamBsSuZ1+uYrELL/6g4yP9EEKJabzBAw23RfkhhKITc6QHlnB3zoIdYfhCuSzfT5yKkg5RqRQSQDpgRRiydsE4hJCCaz1JMQOy6N5ysIB0Xtn5jr8q1wNbmhqkbpq7LcrPaoB+2f3GqITMuRtTzY31OjoFLG7aVoW6DzLRL66sP3W5UuaTSRQE//nkGZ/8i+whjPppI4Ra+O00YxskjPHt8Sv4EVYCJCQBjJUZn9j17mjnTi7d61t+n+k5FD9YevexTAvqYG2SumkO3pQghGKScrE5ITdiilr57aqTQgLIBKwIQ2aOG6r4ClRSzIAsunem8tjtvNLKb9+fVSz2p6s4ePK6mLgYL4Rv3muoa2pD7FWNIIT0tDXn+Q5HNHxQorEQ71UHyRIY7yEQQs2tL+Xrk/8ophlFWB3qjhA6fv42uXTUAAmzOJClqcE7s91edAjmxKYyOFzSa5J27i7L97+/+zSDSEkrv50kdOBtgdD269LuxYh2jSHelGCnIjEnBMfIZPydxAqwkQHOqpBCAsgErAhDWFmBSooZtPLbC0pr8H1XO3MWO6+IqvXRzHOlD3UhjB9hq4gdQ0LcdCr18r5eiO+ET3HEd8gSGKudI4SGWZsghO7VPaW5ZxIr2B7sZb946qjHzXxqZEguVgS54KlZ3gMlYT5/l5aGBln+HzhdGpWQabFgt0xvEoGaa0uNxlG//OkF5TgVOOPCbZoDw05FbE6oN+pzIgOcITWrt6AcVSL1AK9AsRQSM8VsHDMQ0hrKK6ne+GP+lYpaPR3NtucdxZX1d2ubnYeysxERUuvDUrVPnvFZ95XhiWbljlP5JTUIobe/yV7/uqfYZgHyIpcEcundBoSQj5NVwspXN4R7i6oQIoRSNgS7/etHhFB0Yo6/mw2zS8HlcuIj/UwMdXel8banFN6ve7o10k8uFSl8xebEprJiSPAGwjEyuTQpIi7C9/1Qd6pGqpCgr3TE5tqSL3/GhdsZW+ZFJ+YU/fWovqlNrqsn9GSqIQHBvV4E7EUUQpEUnXEjBnV0Cqj7mEeNLUu2nSD9g49vDmWc2yoWErH0Hf0KjjPjgglqS236C1WZWJoapGyYvTzQBSF0q7qRxZQh+oIo6QXlCCE3ewssFSp2CWw1gB2dcx0tjU/DvU/GzRtsbnjovzcZ7FCxwwffL69RKHFDCCFdE1FvkiQiA5wleVODvewRQkV/PWp4+jLgpGDNPAju9VLAiigE1UdM05DgPBwfJ6vls1wQRdju6Jmyie8fOll4h9o/WMHcViFEI5ZYrpW01MYL2JikXLaqDXS0NDYu9rEaoI//ZCtliL5TK72gAiEUEfAPaWEpCqHUCDwz3BwsYhf7IKZFJMT1F70zR2xvdgWRZEol3eIifCV5U6nXjfWaeaAXAVZEUagxTDpTZFr+y9XxbG8HLGyXmM6LSsh8e2f2k5bns73tL+xctG6BJ9aHp1azsyXYR03AF+uFGzvMAlcb7D3BJKFWCC1NjXD/UeRPVlKG5FX57adD17PEym6J7FAZZF5g6UM9Hc2q2mZJvdlZQazkNmMRblwzTz80AqgTYEVYQC5D8n1mMUIoImA0kXTdnlKYll/eX19n3wczkj4MEHIWrwp1H2ZtUtfUxq5gnxSSPwzYFuXXX19nww/n6CTUygQLxZM/8VVSsKZdplMLr47lgnRhUrDABe9QmWVeYCvy/ZoAXS0N3Jud2RiIuAtbHSelQ7xb7GZqAL2C3mdFiEdIpcqR5N2R4NWxv9sQPR1NhFB/fZ1jsSFiW0DqaWt+9/40FgX7ZIKTrPAGghUxdiIU72pn7uNkhRC6/aBpytqU8PjjJZUMtRplOrU8R75MEqVfuXbw40BWGocgSu7vN6k8Br5BV7uBn0X4IoQ+O3iB2YdOWkv1jJeJbFkUdwkCvY5eZkWIFgiOlyp7OP+AakjoxBVa+e3hWzPanncghCa5DpaSy0sq3VbuOMV42pUXMk2LngiDQhMsFF/1qPnHdbMOxgTisvasS1UBMb/8eKqEwfCkO7Va+e37/35ZY30dml8VatmH4v7DYC/7d2a7YeEpmlM5tXZ9yTSneROHdXQKln+dxcDJRnyVPdbkQzRTA+gjaI6MSHrc3EPLB8WbaB49W4bzl6JD3KQ8TVnNn7Ehefub7HPF1VEJmVPH2iSsnEx+z638djKb7D1xLeNCRdn9RpzOm325SjTll8rqUPerFbV42v0swnfJNKfuPhc3B4stSydu+OGc6IkwwN9tiLWZQXX9s9zi+0Hj7f1cBn+ecnFXGo/f3vnhd3/ml1R/sWKygZ62XK8ppVn9hh/O5ZfUaGtyX3QI9p8qXjrDiebgccOPKxW1cQfPb146Ua7xCMHlcmIWjh8y0HD9vrPRiTnuwywcrE2kPH9/VjFOf/h82SS8efpixeSSqsc37zUwaPOODZIGl4PDM4o096VJ4Hi7mKTcvSeufTDPQ5VFFgDW4ZIsvR5A8Saa+Ge2fdkkOl9T1uvp6CCkYkTVeLBZtIdU/CUcKyq73+jn/MrxzaHejlYyFTi0NDV2vzdt5riheNpd+XXWs7YX3X0uS2eMZkuOSVODO8dnGPrbRUaSYm0tjBBCx87d8vvgcPyhC3LttCS1rsorqf4p+4aejmbGZ/PkzcNmt+HH38JTvkhW3D6vpHrNnjO6WhprF4wjSXQGetopnwYzUzHBSRyv+Y1gy0cnilD0SJFFBtCr0ezqQkJVo91EDq8qOjGHcYEeokREZOoixEf60i+qYh08d8z0HLozlSdU4GbUT7u59YWuloa/m81sH/vp7rYGetqRM50LSmsS03gRM5ylLBiN9XV+XD/ru+NXN/6Uf+zcrZKqxymfBjO4kvRrDMmJ4FK44A2/fbrIG8dRGRDiY78rnYcjGXramgghNweL9M1z8Yvfq3uacKwo4ViRvVX/EG+H2d72TrZm0l8Qt67KL6mJP3Rha5QffpD4PCNmOI+xH5iwcjJ+ffrLeew/3J5SuPzrrE1LJiheKRk43m7viWu4GwcpB6FCxhw502XdAk/qv6iliHLtSHASx9oF43jltVEJmZt+KtDV1gwcb8fWXC+lhRT+gkGbwr4Dl8Ohm/On4O01vxGKaGjLFREhoUUlQiSD8O3S7sXOQ82aW1/019fJ+GzeD+tmhk4cjn04MntZU1kR5Hris9DB5obY0SHXlWTmucYTGVVsXK7DCWIjGfjFh1FcPRU1TxKOFU1ec8TrvQMyX1O0ddWGH85RfZ7yZj1gVoe6L5nmVF3/jJVKSdL4j9TlUKFqNov10zI7BUw/HS3SrJBIvyi4Qc8rqX5/92mbRXtEW0jhL5j3qoP1TW0gytt34PbYZ0wt0GNQ1EYzIoIhC64cXlX3ZdzTAVvQsvsNkV9mXr9Tj3OxhALpJOWXVCBKx83BIuOzUAbTCqkxlFfQXshNJ9exVMSm51qaGpyMmzdz3Et1WHNjvSAvO6N+2hU1T14Oe8OxdXvPpOWXi05/Qir6xJdF1T9mMAtraWrERfiyKK5uaWqAd/xEcJDq55Sp2ayIIcHNCqn16ow9vXkl1SGxv82JTT1wuhSJK2sPHG+H38UxMhlEefsOnNFR31/fu7TH3u9BwzMcfEYIJUb7U5txSgcHzDO2hHqOtKT/fHnfhf4r0wzdt/LbN/xwDovajrIZsPMdf7G5WAJB16QPD9+81xC72Ds6ZCydYTxoeIYdHXaWxvQdHftOXsO+vvhIX+zfoJ6OzFN70PCMuOkYJC/wyh9N/+gXfV2tG/veEvXdYWddR6dg5GDTAzGBZfcawuKl7XvCpzi+OmbIaFszvw8OvegQvDvHLauosux+4zuz3TYu8REdOZGoIudO/ivpxCUdxSx9o76pDQuXCYGVB2UKbpLB2Aw0wq5FKRO02BFi4yF2DPQx6qe9Isg1fMooS1N9sQOgnmbPZ7gAPQ9nwqoD574O68m3fN7eeSDnxvp9ZxFCBTvCpCeuYPJKqrFiNv0vJVUYnOa70IT+DIKdFfklNYZ6WiuDx6wIdMUV6WJJyy+PSsgcYKR7efcSmuk0DAwJ1Qzgo7C0LU0rgsGTETO9vHHv/FT5qPmL5ZPenD5a9L+88keRX2beq3tKHdvhT4JK7z7OK6kpKnv4RKQdvRBzJwyLi/AVO37Rc8dXrJXfjqXIxZ642KMYJwGK3QTQv5LURZiUxDn8e/Fxsvotdo7oK0syZjIZP9LSz+UV6V9j6luoTmtnoFvh+K89kv35gp5/Y7woXjbLRWy8kUBdy3++bNKb05xofinx73ztgnHbUwpFJ1nR5hMIoQ/meQR52UlXzy2vbvReddDWwih/R5hYhXDqyLEJEevFEoXBdgQx3ZGUVzeGb824/aBpYP9+tU9a8XRjsWA36v6m7p+nXNyeUuhqZy7pW0fOyNbCqPJRM6JM1gJBF0kpFHR1UUVqMa525sdiQ6TPceTc0d/bi6yiyjV7zkQGOMdF+Eo6cepRBKUstPEi7Ktjlx42tojdlLTy20NiU69U1Irdk2GYebTkWjfUN7WBLm8fQcN5atjCVx17/o0tB+h/+/vVy7cemRrqWg7QNxRXK4An4owLtw31tFaFui+eOgpXetNhe0ohQmjL0om5xfdvP2jKLKoM8rLD7yLWhCCECkprDpwuFXR1eY4YpMEVX4/5fVbxueJqjxGDFkyS4SVbv+9sxoXbNE0IQojD4Qww0ksvqHjczF/kP4rmz89QTzvIyy6zqBKfo5ejpYWJvsyjTI308FH3658hhO7VPf3iaCH+19rXPPX1urGwwNbCeO+Jaw8aWqa72w4yFTNUfEbZl++SKTs6xA1vzjgcTj9dLXzT19Vyc7BY5D8qYoZzdIjbLM+hKWfKHja2zHC3tTYzlDIAfO4dnV2Xbz3K4d399verWZeqEEJblk58xVzigdSjyINkYD2JpgbXzcFi7kSH63fqr9+pTy+ouFJRO8HJivyCDp4u/TH7xiibAV+umCxpeOQyynXjcOQwCfI+H+i9cMLifj8QE6SU9yY7a7E7EtJmg/5ETIX4HNo7O4VW65Tebf9bSAq6ur7LuLorjdcp6ELiXOeYuRtTzxVXf/POFOmmF7sU9HQ0j28OpT9ygaDLM/rnu3XNv22cM8HJmv7JPmh4tnjriau36zQ1uKvnua8OdZe+TyJHkU5NL4fdIy6I8PjjWZeqlkxz+nLFZEnPaXzGX/FV1n+v3kMIvTd37CcLvWSOCu9yJo62PvxJsI6W7NMvr24kvUbon3h9UxtuUvJ+qLuZkTIFzKmbEtH/0o8gAoCCcCK+OJn0YYCy3h47iNA/QxePGlvW7jlzsvAOkhqRlg7Vc011++AVrqRZg1f+aFf6FVyxZTVA/43JjkKFC/hlr+15U7rvaN3eM99nFktxKUjig//896fsG2sXjBOqG5DJ8/ZOXA3eKejyGG6RvCaAjndLyKD2jAsis6hy0dYMfV2t4r1LpdSrCwRd2MmGEJo61uY/q6ZJd1W1d3SOi/65uv7Z92sDZHavIigS41EFqGEbAgQkgJ6EKzNQ1q04WJsIFTGIbbOh4LtQEyXxI6aG4leRbg4W+z6YUbAjLMTHoeZxCy5cOH3lrtDTtDRkLHWJcK+8Q/Vxskb/1FOiCbUavOivR7gygGa/KVPDHm0N5O82xMfJqoXfvu3IRSlPI4PR1OBmX67yX5ty6lKllOdraWpQy+Np0tt7IgmVJeEbmBCgJ+FqaihZkJEUMZRU1i/amiG2zYbiYEOCk9nzvl4ovUrGwdrk2/emHowJfGPySITQ61t+J9n9cr0p/bYWhAlOVgihorKHzEp/cTU4qQyIScplpSsUu2hqcLcsnUhfZSRlQ7CthVFVbXNYfMairRl3a5slPZNoovS1wmlmTUEAgBV6rupQEqQga/KaI5lFlQOMdMW22WDljfCqjc5KTUtTY5q77c5of1F5GJlGSMFBLpg04kWHIP7wBcavgE+zYEcY3n5RtbwYyPF2B0Sl+P1v/ytzxnccMiBn++srg1w1uJzMosoJqw5+cbRQbPssNwcLOqJkAACwiKay9iLP2ztvVNVfqai7fqeusOwheTx04nCxbTbYQl7jZGasJ2RIutuHvjLI9ddzt77LuBrsZc84QIrXpKmb5og6zZHCdWessDrUPT2/vOx+Y3LmdZmZzUb9tDcvnRg2xXH93rMFpTXbjlz89dyt5DUBIwebCj2TpigZAABsoYS9SF5J9dLPT9ou2jP9o1/W7T3zU/aNm/caxtgPxBk7DPQ5uhvG7gJmbebkWqdLR6zTXMgoduvWSgpamhprF3gi2rovCCHHIQPSN8/d/e7Ugf373apuDIj55fgF4RCIXKJkAAAojsa8N99xtR/YM2+WV1L97q6c7SmFt6obuVzOKJsBMzxsF/mPWrtg3KpQd/dhFqaGujm8u0l/XJdSRELzjQ7/edPHyeqt6aOVkrTe0OEvfVIAAASoSURBVMznldea99djtpnwHDHo+PmKv6objfW1Fc/XFE38xzUW+GZtZqgsN/pwa5Pj5yvu1T2VdJq46EeoLMPJ1ixsimPxnfq/qhvTC8pfdAgmOlmTT5mU3RRX1s/zHc74KwQAAE00lq5cLeoWYIvn7Z3X79T9UVj5U3bJlgPnE9N49+qeGvXTfm/u2B/WzlwR5DrDY6ibw0BLUwPsWLMcoE8tB2PmlGjlt6/ccephY8tsb4dXxwzphtOSTQu/Pb2gooXfvsh/FIPDNbhcMhVGzHCmU/whF4xLydhF5owv1ooghPS0Nef5Dn/e3nHh5sPzpQ+uVNROG2ujq/2yInW4tcmVitqSysfUUlMAALoJzh+Fd2Z42LL4is2tLy6U1uSV1OSVVBdX1nd0/s+rY21mEDbFUaYOD7UcTF4JLKI7MspmwLH/C1GWEhypdWAs4cVMEKXXIRB0hW/NyL5cJVbBRaZcVVp++bu7ctqed9hZGu9fN4ush0iFkEyJHQAAFIRzrvi+XGXSkrhb25xeUJ6WX3H9Tl3n32LsmhrckYNN3RwGOg81H2NvPsrGjE5RMYaozyLJleQE3IyWdBJkVu7OLnj8cslbCYH1GT2GW5yMm8/68FQHKTM+HdHD0ruPF289UVXbrK+rlfiuP6k3JJX5ICsLAN0Kh1f+aIwCcRFiPK5U1OJHtDW5HiMGTXCynuBk7TF8EH2zIYTYolw60NTZ7m6Y6SRSIRsatZ8HJc34NKVziVwKh4NWzXWPeWM8jvTgwxOj/V/zGwElFADQTXBK7z6WFBfBFiKHdxcrUUtHqAssW+MjskUyzUlkgHPoxGF2lv1VR81CcUPCWIG814HPlOoApC+fjBASCLq2HCzYmcpDFLmU3Ov3QzeloW7oMQMAAIFjNi9RkeMHmxu6DDVn3XiIIrNQTnWMBxUFDQmeW4v3LqWj1NurEXUAfvlL4dbDF6eOtTn0MV21UBImsbUw2hDuPd3D9tDpUtzJpi9cQwBQCtKsCLEQE5ysJSml9zfQUbqGioqjiCGJP3Qh4VjRyiDXzUsndt8IVQHRq0RTPlmI0ruPV+44daPqMUJo5GDTNa+Nyyup/j6zWL2TFABAiXDqnrRK+h9YCLZgbEiu36mb/tEvnQLB8c3qL/QtdJVwpESmfLIoWDJ9x2+Xah63IIRwJy7UNxyDANDzcLq6upQ9hj4BtYM3A4K87L5fM5PdIakgoleJ8dRPbb+hr6vVwm9HbPdOBgAAIaSxceNGZY+hT4Bb+Ak1y6PPbxvn6PcBVSihqxQZ4BzkZc+sLpL0BMwsqqz9e8PN5XL83WzYHDEA9HlgL9LT9EDLazWgvqktOfN6gMdQxTO2qSnjJfveGti/HxsDBADgJWBFgD5Bb+9pCAAqC1gRAAAAgDmQggUAAAAwB6wIAAAAwBywIgAAAABzwIoAAAAAzAErAgAAADAHrAgAAADAHLAiAAAAAHPAigAAAADMASsCAAAAMAesCAAAAMAcsCIAAAAAc8CKAAAAAMwBKwIAAAAwB6wIAAAAwBywIgAAAABzwIoAAAAAzAErAgAAADAHrAgAAADAHLAiAAAAAHP+H+P7Hz4xpKS5AAAAAElFTkSuQmCC"
    # :Flags => "FORCE_DECLINE"    
  })
  puts "Sale result:"
  puts JSON.pretty_generate(result)  
  
  # refund
  result = Netsecure.Transaction.post(
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'CREDIT',
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    :PaymentID => "",
    :TotalAmount => "1.00"    
  })
  puts "Refund result:"
  puts JSON.pretty_generate(result)
=end

  #void
=begin
  result = Netsecure.Transaction.post(
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'VOID',
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    :RefTransactionID => "201509232239488000"  
  })  
  puts "Void result:"
  puts JSON.pretty_generate(result)
=end

  #search payments
#=begin
  result = Netsecure.Transaction.post( q=
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'ADMIN',
    :Action => "INQPMT",
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    #optional
    :SearchDateFrom => "2015-09-16 00:00:00",
    :SearchDateTo   => "2015-09-23 23:59:59",
    :Page           => "1",
    :ResultsPerPage => "10",
    :SearchTerms    => "Public",   
    :SearchFilters => {
      :Filter => [{
          :Field  => "CurrentAmount",
          :Mode   => "KEYWORD",
          :Value  => "1"
      },{
          :Field  => "CardType",
          :Mode   => "KEYWORD",
          :Value  => "VISA"
      },{
        :Field => "Username",
        :Mode => "EXACT",
        :Value => "demo@netsecure.ca"
      },{
        :Field => "TransactionType",
        :Mode => "EXACT",
        :Value => "SALE"
      },{
        :Field => "PaymentStatus",
        :Mode => "EXACT",
        :Value => "COMPLETED"
      },{
        :Field => "Approved",
        :Mode => "EXACT",
        :Value => "true"
      },{
        :Field => "MaskedPAN",
        :Mode => "KEYWORD",
        :Value => "41111"
      },{
        :Field => "ApprovalCode",
        :Mode => "KEYWORD",
        :Value => "TEST90"
      }]
    }
    #:Flags  => "DETAIL"  
  })  
  puts "Search payments result:"
  puts JSON.pretty_generate(result)
#=end

  #search transactions
=begin
  result = Netsecure.Transaction.post(
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'ADMIN',
    :Action => "INQTRX",
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    :SearchFilters => {
      :SearchDateFrom => "2015-01-01 14:00:00",
      :SearchDateTo   => "2015-01-07 14:00:00",
      :Page           => "1",
      :ResultsPerPage => "10",
    }
    #optional
    #:Flags  => "DETAIL"
  })  
  puts "Search transactions result:"
  puts JSON.pretty_generate(result)
=end

rescue Netsecure::NetsecureException => ex
  puts "Exception: #{ex.user_facing_message}"
end

