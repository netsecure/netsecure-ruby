module Netsecure
  class Account
    include Singleton

    def create(data)
      request = Netsecure::Request.new()
      val = request.post(gateway_url, data)
    end

    private

    def gateway_url
      "#{Netsecure.api_base_url()}"
    end
  end
end