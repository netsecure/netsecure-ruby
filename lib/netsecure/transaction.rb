module Netsecure
  class Transaction
    include Singleton

    def self.generateOrderId
      # 10/05/2015: mg: must be 18 digits long
      Time.now.strftime("%Y%m%d%H%M%6N")
    end

    def post(data)
      request = Netsecure::Request.new()
      val = request.post(gateway_url, data)
    end

    private

    def gateway_url
      "#{Netsecure.api_base_url()}/payments/"
    end
  end
end
