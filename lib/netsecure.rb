require 'singleton'
require 'netsecure/account'
require 'netsecure/transaction'
require 'netsecure/request'
require 'netsecure/util'
require 'netsecure/exceptions'

module Netsecure

  @url_prefix = "sandbox"
  @url_suffix = "Gateway"
  @url_handler = "GatewayHandler.ashx"
  @url_base = "netsecuretechnologies.com"
  @url_version = "V3"
  @ssl_ca_cert = File.dirname(__FILE__) + '/resources/cacert.pem'
  @timeout = 80
  @open_timeout = 40

  class << self
    attr_accessor :merchant_id, :payments_api_key, :profiles_api_key, :reporting_api_key
    attr_accessor :url_prefix, :url_base, :url_suffix, :url_version, :url_handler
    attr_accessor :url_payments, :url_return, :url_void
    attr_accessor :ssl_ca_cert, :timeout, :open_timeout
  end

  def self.api_host_url()
    "https://#{@url_prefix}.#{url_base}"
  end

  def self.api_base_url()
    "/#{url_suffix}#{url_version}/#{url_handler}"
  end

  def self.Transaction()
    Netsecure::Transaction.instance()
  end

  def self.Account()
    Netsecure::Account.instance()
  end
end


def run()
  #Netsecure.merchant_id = "300200578"
  #Netsecure.payments_api_key = "4BaD82D9197b4cc4b70a221911eE9f70"
  #result = Netsecure.PaymentsAPI().make_creditcard_payment(12.5)
  #puts "Payment result: #{result}"
end
