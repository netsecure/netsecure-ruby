# NetSecure's Ruby SDK

Integration with NetSecure’s payments gateway is a simple, flexible solution.

You can choose between a straightforward payment requiring very few parameters; or, you can customize a feature-rich integration.

# Setup
To install the SDK you just need to simply install the gem file:
```
gem install netsecure --pre
```

# Code Sample
Take a credit card Payment:
```ruby
begin
  result = Netsecure.Transaction.post(
  {
    :TransactionID => Netsecure::Transaction.generateOrderId(),
    :TransactionType => 'SALE',
    :Username => "demo@netsecure.ca",
    :Password => "demo",
    :TotalAmount => "1.00",
    :EntryMode => "KEYED",
    :CardNum => "5555555555554444",
    :CVV2 => "123",
    :CardExpMonth => "12",
    :CardExpYear => "18",
    :CardName => "Test Holder"  
  })
  puts "Search payments result:"
  puts JSON.pretty_generate(result)
  
rescue NetsecureException => ex
  puts "Exception: #{ex.user_facing_message}"
end
```

See test.rb for more samples

# Reporting Issues
Found a bug or want a feature improvement? Email NetSecure support support@netsecure.ca