#$:.unshift(File.join(File.dirname(__FILE__), 'lib'))


Gem::Specification.new do |s|
  s.name        = 'netsecure'
  s.version     = '1.0.3'
  s.date        = '2015-09-22'
  s.summary     = "NetSecure Ruby SDK"
  s.description = "Accept payments using NetSecure and Ruby"
  s.authors     = ["Maurice Bujold", "Michael Gauthier"]
  s.email       = 'mo@netsecure.ca'
  s.homepage    ='http://netsecuretechnologies.com'
  s.license     = 'MIT'

  s.files = `git ls-files`.split("\n")
  #s.test_files    = `git ls-files -- test/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ['lib']

  s.add_dependency('rest-client', '~> 1.4')
  s.add_dependency('json', '~> 1.8.1')

  s.add_development_dependency('shoulda', '~> 3.4.0')
  s.add_development_dependency('test-unit')
  s.add_development_dependency('rake')
end